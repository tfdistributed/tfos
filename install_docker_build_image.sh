#!/usr/bin/env bash

#https://github.com/NVIDIA/nvidia-docker
# Add the package repositories
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | \
  sudo tee /etc/yum.repos.d/nvidia-docker.repo

# Install nvidia-docker2 and reload the Docker daemon configuration
sudo yum install -y nvidia-docker2
sudo pkill -SIGHUP dockerd

sudo usermod -a -G docker ec2-user
# logout after modifying user groups
# login
sudo service docker start

# Test nvidia-smi with the latest official CUDA image
docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi





#https://github.com/uber/horovod/blob/master/docs/docker.md
mkdir horovod-docker
wget -O horovod-docker/Dockerfile https://raw.githubusercontent.com/uber/horovod/master/Dockerfile
docker build -t horovod:latest horovod-docker

ssh-keygen
#save key files to default locations
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
cp sshd_config ~/.ssh/

docker run -it --runtime=nvidia --rm -v /home/ec2-user/.ssh:/root/.ssh horovod:latest
#/usr/sbin/sshd -f /root/.ssh/sshd_config -p 7777
#ssh 172.17.0.2 -p 7777
