## LSTM for review sentiment analysis demo

Various backends used for the task:

* plain TensorFlow on a single GPU [general_lstm.ipynb](general_lstm.ipynb)  
* Jupyter notebook [distributed_lstm.ipynb](distributed_lstm.ipynb) to run pre-processing on one machine
* and distributed model training with Horovod: [horovod_lstm.py](horovod_lstm.py)

