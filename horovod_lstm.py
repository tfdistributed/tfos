import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM
from sklearn.model_selection import train_test_split

import keras
from keras import backend as K
import tensorflow as tf
import horovod.keras as hvd

npzfile = np.load("/mnt/share/reviews_checkpoint_20k.npz")
X = npzfile['X']
Y = npzfile['Y']
vocabulary_size = npzfile['vocabulary_size'][0]

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
print(X_train.shape, Y_train.shape)
print(X_test.shape, Y_test.shape)

#Horovod-specific >>>
hvd.init()
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.visible_device_list = str(hvd.local_rank())
K.set_session(tf.Session(config=config))
#Horovod-specific <<<

embed_dim = 50
lstm_out = 80
model = Sequential()
model.add(Embedding(vocabulary_size, embed_dim, input_length=X.shape[1], dropout=0.2))
model.add(LSTM(lstm_out, dropout_U=0.2, dropout_W=0.2))
model.add(Dense(2, activation='softmax'))
# /home/ec2-user/anaconda3/bin/ipython:2: UserWarning: Update your `LSTM` call to the Keras 2 API: `LSTM(80, dropout=0.2, recurrent_dropout=0.2)`

opt = keras.optimizers.Adadelta(1.0 * hvd.size())

#Horovod-specific >>>
# Horovod: add Horovod Distributed Optimizer.
opt = hvd.DistributedOptimizer(opt)
#Horovod-specific <<<

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=opt,
              metrics=['accuracy'])

print(model.summary())

#Horovod-specific >>>
callbacks = [
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    hvd.callbacks.BroadcastGlobalVariablesCallback(0),
]
#Horovod-specific <<<

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
if hvd.rank() == 0:
    callbacks.append(keras.callbacks.ModelCheckpoint('./checkpoint-{epoch}.h5'))

batch_size = 32
epochs = 5
model.fit(X_train, Y_train,
          batch_size=batch_size,
          callbacks=callbacks,
          epochs=epochs,
          verbose=1,
          validation_data=(X_test, Y_test))
score = model.evaluate(X_test, Y_test, verbose=1)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save("trained_model_20k_5epochs_1gpu")

